package edu.sjsu.android.intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


public class ActivityLoaderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);

        Button webButton = findViewById(R.id.webButton);
        Button callButton = findViewById(R.id.callButton);


        webButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri webpage = Uri.parse("https://www.amazon.com");
                Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);

                String title = getResources().getString(R.string.chooser_title);
                Intent chooser = Intent.createChooser(webIntent, title);
                if (webIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(chooser);
                }
                startActivity(chooser);
            }
        });


        callButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uri number = Uri.parse("tel:+194912344444");
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
            }

        });

    }

}